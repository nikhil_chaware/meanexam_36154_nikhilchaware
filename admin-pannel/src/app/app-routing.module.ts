import { LoginComponent } from './login/login.component';
import { SearchEmployeeComponent } from './search-employee/search-employee.component';
import { GenrateSalarySleepComponent } from './genrate-salary-sleep/genrate-salary-sleep.component';
import { QuerriesComponent } from './querries/querries.component';
import { MeetingComponent } from './meeting/meeting.component';
import { LeavesComponent } from './leaves/leaves.component';
import { AddNewEmployeesComponent } from './add-new-employees/add-new-employees.component';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path:'home', component: HomeComponent},
  {path:'add-new-emp', component: AddNewEmployeesComponent},
  {path:'leaves', component: LeavesComponent},
  {path:'meeting', component: MeetingComponent},
  {path:'querries', component: QuerriesComponent},
  {path:'genrate-sleep', component: GenrateSalarySleepComponent},
  {path:'search-emp', component: SearchEmployeeComponent},
  {path:'login', component:LoginComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
