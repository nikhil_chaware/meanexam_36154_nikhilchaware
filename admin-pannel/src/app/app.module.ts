import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AddNewEmployeesComponent } from './add-new-employees/add-new-employees.component';
import { SearchEmployeeComponent } from './search-employee/search-employee.component';
import { MeetingComponent } from './meeting/meeting.component';
import { LeavesComponent } from './leaves/leaves.component';
import { QuerriesComponent } from './querries/querries.component';
import { GenrateSalarySleepComponent } from './genrate-salary-sleep/genrate-salary-sleep.component';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AddNewEmployeesComponent,
    SearchEmployeeComponent,
    MeetingComponent,
    LeavesComponent,
    QuerriesComponent,
    GenrateSalarySleepComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
