const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const config = require('../../config')
const jwt = require('jsonwebtoken')

const router = express.Router()


//get
 
//view leave status

router.get('/view_leave', (request, response) => {
    const statement = `
        select l.leaveId, e.empId, l.leaveStatus
        from  e employee, apply_leave l 
        where l.leaveId = e.empId = ${request.userId}
    `
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })


// post api



  router.post('/signin', (request, response) => {
    const {email, password} = request.body
    const statement = `select id, empName from employee where email = '${email}' and password = '${password}'`
    db.query(statement, (error, users) => {
      if (error) {
        response.send({status: 'error', error: error})
      } else {
        if (users.length == 0) {
          response.send({status: 'error', error: 'admin does not exist'})
        } else {
          const user = users[0]
          const token = jwt.sign({id: user['id']}, config.secret)
          response.send(utils.createResult(error, {
            firstName: user['firstName'],
            lastName: user['lastName'],
            token: token
          }))
        }
      }
    })
  })

  // put 

 //edit profile

router.put('/:id', (request, response) => {
    const {id} = request.params
    const {empId, empName, address, birth_date, gendor, mobile, password,role, } = request.body
    const statement = `update employee set 
    empId = '${empId}', empName = '${empName}', address = '${address}', birth_date = '${birth_date}', 
    gendor = '${gendor}', mobile = '${mobile}', password = '${password}', role = '${role}' where id = ${id}`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })



//post 
//apply leave

router.post('/apply_leave', (request, response) => {
    const { leaveDate, leavePurpose, empId } = request.body
  
    const statement = `insert into apply_leave ( leaveDate, leavePurpose, empId ) values ('${leaveDate}','${leavePurpose}','${empId}')`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })


  module.exports = router
