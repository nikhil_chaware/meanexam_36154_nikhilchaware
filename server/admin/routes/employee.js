const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const crypto = require('crypto-js')


const router = express.Router()

//get api

//get view emp applied leave 

router.get('/leave', (request, response) => {
    const statement = `select * from apply_leave`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })


  
  //view emp querries

  router.get('/emp_queries', (request, response) => {
    const statement = `select * from emp_queries`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })

//post api

//add new employee

router.post('/addNewEmployee', (request, response) => {
    const { empName, address, birth_date, gendor, email, mobile, password, role } = request.body
  
    const encryptedPassword = crypto.SHA256(password)
    const statement = `insert into employee (empName, address, birth_date, gendor, email, mobile, password, role) values (
      '${empName}', '${address}', '${birth_date}', '${gendor}', '${email}', '${mobile}',${encryptedPassword}, '${role}'
    )`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })

  // approve emp leave
  router.post('/aproveleave', (request, response) => {
    const { leaveStatus } = request.body
  
    const statement = `insert into apply_leave ( leavestatus ) values ('${leaveStatus}')`
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
  })



  module.exports = router
